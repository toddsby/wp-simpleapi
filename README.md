#Wordpress Simple API
---
Simple Wordpress API to output data from Wordpress database to JSON (semi-RESTful)

See more: <http://www.restapitutorial.com/lessons/restquicktips.html>

 * GET, POST, UPDATE Flickr Usernames (stored in Wordpress options table)
 * GET, POST, UDPATE Flickr API Keys (stored in Wordpress options table)
 * POST Flickr post_meta (stored in Wordpress postmeta table)
 * Wordpress Simple JSON Auth
 * GET post_type => recipe posts (retrived from Wordpress posts table)

Author:
[@drewtoddsby](http://twitter.com/drewtoddsby)