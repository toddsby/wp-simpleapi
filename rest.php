<?php
/**
 * Wordpress Simple API 
 * @author Drew Toddsby
 * @version 1.0.1
 * @license MIT
 *
 * Simple Wordpress API to output data from Wordpress database to JSON (semi-RESTful)
 * See more: http://www.restapitutorial.com/lessons/restquicktips.html
 * 
 * GET, POST, UPDATE Flickr Usernames (stored in Wordpress options table)
 * GET, POST, UDPATE Flickr API Keys (stored in Wordpress options table)
 * POST Flickr post_meta (stored in Wordpress postmeta table)
 * Wordpress Simple JSON Auth
 * GET post_type => recipe posts (retrived from Wordpress posts table)
 * 
 */

/* 
Template Name: RestAPI Auth
*/

// URL Parameters switch 
// jpost ie JSON POST
// Example: http://recipefordisaster.us/wpbl-admin/api/?f=flickr_username&action=get_flickr_user
switch($_GET['f']) {
case 'login':
    jpost_login();
	break;
case 'logout':
	jpost_logout();
	break;
case 'getrecipes':
	jpost_get_cpt();
	break;
case 'flickr_username':
	jpost_flickr_username();
	break;
case 'flickr_key':
	jpost_flickr_key();
	break;
case 'flickr_save':
	jpost_flickr_save();
	break;
default:
	$jpost_response = "Not found";
	$jpost_status = "Error";
	$jpost_http = 500;
	$jpost_status_text = 'Not found';
	// date always returns null because $today is an empty variable, return messages are only partially implemented @TODO
	echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'recipes'=> 'none'),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 500 Internal server error");
	break;
}
// utility function for handling duplicates
function jpost_duplicate($currentdata,$newdata,$key=0) {
	$duplicate = 0;
	foreach ($currentdata as $data) {
		if ( $data[$key] == $newdata[0][$key] ) {
			$duplicate = 1;
			break;
		}
	}
	return $duplicate;
}
// base function for handling flickr user data
function jpost_flickr_username() {
	$jpost_filtered = array();
	$actions = array("get_flickr_user", "set_flickr_user", "update_flickr_user", "update_selected_user");
	if (isset($_GET["action"]) && in_array($_GET["action"], $actions)):
		switch($_GET["action"]):

		case "get_flickr_user":
			// get_option is a wordpress utility function for retriving data from the wordpress options table
			// See more: https://codex.wordpress.org/Function_Reference/get_option
			$jpost_db_flickr_username = get_option( 'wpsimpleapi' . '_flickr-username' , false );
			if ($jpost_db_flickr_username == "") {
				$jpost_response = 'No users found';
				$jpost_status = 'Not found';
				$jpost_http = 404;
				$jpost_status_text = 'Not found';
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 404 Not Found");
			} else {
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('username'=>$user['username'], 'nsid'=>$user['nsid']);
				}
				$jpost_response = 'OK';
				$jpost_status = "Success";
				$jpost_http = 200;
				$jpost_status_text = 'OK';
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
			}
		break;
		case "set_flickr_user":
			$jpost_filtered = array();
			// $jpost_data = $_POST['data'];
			$jpost_data = json_decode(file_get_contents("php://input")); 
			// for handling raw json post data whose headers are not set to x-www-form-urlencoded
			// See more: http://victorblog.com/2012/12/20/make-angularjs-http-service-behave-like-jquery-ajax/
			// if not using angular on the front end $jpost_data = $_POST['data'];
			$jpost_ang_flickr_username['selected'] = array('nsid'=> $jpost_data->selected);
			$jpost_ang_flickr_username[] = array('username' => sanitize_user( $jpost_data->username, true ), 'nsid' => $jpost_data->nsid);
			$jpost_check = get_option( 'wpsimpleapi' . '_flickr-username' , false );
			if ( is_array($jpost_check) ) {
				$jpost_db_flickr_username = $jpost_check;
			}
			if ( isset($jpost_db_flickr_username) ) :
				$duplicate = 0;
				foreach ($jpost_db_flickr_username as $user) {
						if ($user['nsid']==$jpost_ang_flickr_username[0]['nsid']) {
							$duplicate = 1;
							break;
						}
				}
			else: 
				$jpost_db_flickr_username = $jpost_ang_flickr_username;
				// update_option is a wordpress utility function for storing data to the wordpress options table
				// See more: https://codex.wordpress.org/Function_Reference/update_option
				update_option( 'wpsimpleapi' . '_flickr-username', $jpost_db_flickr_username );
				$jpost_filtered = $jpost_db_flickr_username;
				$jpost_response = 'OK';
				$jpost_status = "Success";
				$jpost_http = 200;
				$jpost_status_text = 'OK';
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
				break;
			endif;

			if ($duplicate == 1) :
				$jpost_response = 'User already exists';
				$jpost_status = "Conflict";
				$jpost_http = 409;
				$jpost_status_text = 'Conflict';
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('username'=>$user['username'], 'nsid'=>$user['nsid']);
				}
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 409 Conflict");
			else:
				$jpost_m_array = array_merge($jpost_db_flickr_username,$jpost_ang_flickr_username);
				$jpost_db_flickr_username = $jpost_m_array;
				update_option( 'wpsimpleapi' . '_flickr-username', $jpost_db_flickr_username );
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('username'=>$user['username'], 'nsid'=>$user['nsid']);
				}
				$jpost_response = 'OK';
				$jpost_status = "Success";
				$jpost_http = 200;
				$jpost_status_text = 'OK';
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
			endif;
		break;
		case "update_flickr_user":
			$jpost_filtered = array();
			$jpost_select = array();
			$jpost_data = json_decode(file_get_contents("php://input"));
			$jpost_idx = absint($jpost_data->idx);
			$jpost_ang_flickr_username['selected'] = array('nsid'=> $jpost_data->selected);
			$jpost_ang_flickr_username[] = array('username' => sanitize_user( $jpost_data->removeu, true ), 'nsid' => $jpost_data->removen);
			$jpost_db_flickr_username = get_option( 'wpsimpleapi' . '_flickr-username' , false );
			if (is_array($jpost_db_flickr_username)) {
				$jpost_db_flickr_username['selected'] = $jpost_ang_flickr_username['selected'];
				$jpost_select['selected'] = array_shift($jpost_db_flickr_username);
				array_splice($jpost_db_flickr_username, $jpost_idx, 1);
				$jpost_m_array = array_merge($jpost_select,$jpost_db_flickr_username);
				$jpost_db_flickr_username = $jpost_m_array;
			}
			update_option( 'wpsimpleapi' . '_flickr-username', $jpost_db_flickr_username );
			if ($jpost_db_flickr_username != ''):
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('username'=>$user['username'], 'nsid'=>$user['nsid']);
				}
			else:
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				$jpost_filtered[] = $jpost_db_flickr_username;
			endif;
			$jpost_response = 'OK';
			$jpost_status = "Success";
			$jpost_http = 200;
			$jpost_status_text = 'OK';
			echo json_encode(array('status' => $jpost_status, 'removed'=>$jpost_ang_flickr_username, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered ),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
			
		break;
		case "update_selected_user":
			$jpost_filtered = array();
			$jpost_select = array();

			$jpost_data = json_decode(file_get_contents("php://input"));
			$jpost_ang_flickr_username['selected'] = array('nsid'=> $jpost_data->selected);

			$jpost_db_flickr_username = get_option( 'wpsimpleapi' . '_flickr-username' , false );
			if (is_array($jpost_db_flickr_username)) :
				$jpost_db_flickr_username['selected'] = $jpost_ang_flickr_username['selected'];
				update_option( 'wpsimpleapi' . '_flickr-username', $jpost_db_flickr_username );
			endif;
			if ($jpost_db_flickr_username != ''):
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('username'=>$user['username'], 'nsid'=>$user['nsid']);
				}
			else:
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				$jpost_filtered[] = $jpost_db_flickr_username;
			endif;
			$jpost_response = 'OK';
			$jpost_status = "Success";
			$jpost_http = 200;
			$jpost_status_text = 'OK';
			echo json_encode(array('status' => $jpost_status,'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered ),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
		break;

		default:
		$jpost_response = "Not found";
		$jpost_status = "Error";
		$jpost_http = 500;
		$jpost_status_text = 'Not found';
		echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'recipes'=> 'none'),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 500 Internal server error");
		break;
		endswitch;
	endif;
}
function jpost_flickr_key() {
	require ('lib/http-response-codes.php');
	$actions = array("get_flickr_key", "set_flickr_key", "update_flickr_key", "update_selected_key");
	if (isset($_GET["action"]) && in_array($_GET["action"], $actions)):
		switch($_GET["action"]):

		case "get_flickr_key":
			$jpost_db_flickr_username = get_option( 'wpsimpleapi' . '_flickr-key' , false );
			if ($jpost_db_flickr_username == "") {
				$jpost_response = 'No api keys found';
				$jpost_status = 'Not found';
				$jpost_http = 404;
				$jpost_status_text = 'Not found';
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 404 Not Found");
			} else {
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('key'=>$user['key']);
				}
				$jpost_response = 'OK';
				$jpost_status = "Success";
				$jpost_http = 200;
				$jpost_status_text = 'OK';
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
			}
		break;

		case 'set_flickr_key':
			$jpost_filtered = array();
			$jpost_data = json_decode(file_get_contents("php://input"));
			$jpost_ang_flickr_username['selected'] = array('key'=> $jpost_data->selected);
			$jpost_ang_flickr_username[] = array('key' => sanitize_key( $jpost_data->key, true ));
			$jpost_check = get_option( 'wpsimpleapi' . '_flickr-key' , false );
			if ( is_array($jpost_check) ) {
				$jpost_db_flickr_username = $jpost_check;
			}
			if ( isset($jpost_db_flickr_username) ) :
				$keyname = 'key'; // for duplicate function
				$duplicate = jpost_duplicate($jpost_db_flickr_username,$jpost_ang_flickr_username,$keyname);
			else: 
				$duplicate = 'winning';
				$jpost_db_flickr_username = $jpost_ang_flickr_username;
				update_option( 'wpsimpleapi' . '_flickr-key', $jpost_db_flickr_username );
				$jpost_filtered = $jpost_db_flickr_username;
				$jpost_response = 'OK';
				$jpost_status = "Success";
				$jpost_http = 200;
				$jpost_status_text = 'OK';
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
				break;
			endif;
			if ($duplicate == 1) :
				$jpost_response = 'Key already exists';
				$jpost_status = 'Conflict';
				$jpost_http = 409;
				$jpost_status_text = get_http_response($jpost_http, $textonly=1);
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('key'=>$user['key']);
				}
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header(get_http_response($jpost_http));
			elseif ($duplicate == 0) :
				$jpost_m_array = array_merge($jpost_db_flickr_username,$jpost_ang_flickr_username);
				$jpost_db_flickr_username = $jpost_m_array;
				update_option( 'wpsimpleapi' . '_flickr-key', $jpost_db_flickr_username );
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('key'=>$user['key']);
				}
				$jpost_response = 'OK';
				$jpost_status = "Success";
				$jpost_http = 200;
				$jpost_status_text = 'OK';
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
		endif;
		break;

		case "update_flickr_key":
			$jpost_filtered = array();
			$jpost_select = array();
			$jpost_data = json_decode(file_get_contents("php://input"));
			$jpost_idx = absint($jpost_data->idx);
			$jpost_ang_flickr_username['selected'] = array('key'=> $jpost_data->selected);
			$jpost_ang_flickr_username[] = array('key' => sanitize_key( $jpost_data->removedk, true ));
			$jpost_db_flickr_username = get_option( 'wpsimpleapi' . '_flickr-key' , false );
			if (is_array($jpost_db_flickr_username)) {
				$jpost_db_flickr_username['selected'] = $jpost_ang_flickr_username['selected'];
				$jpost_select['selected'] = array_shift($jpost_db_flickr_username);
				array_splice($jpost_db_flickr_username, $jpost_idx, 1);
				$jpost_m_array = array_merge($jpost_select,$jpost_db_flickr_username);
				$jpost_db_flickr_username = $jpost_m_array;
			}
			update_option( 'wpsimpleapi' . '_flickr-key', $jpost_db_flickr_username );
			if ($jpost_db_flickr_username != ''):
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('key'=>$user['key']);
				}
			else: 
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				$jpost_filtered[] = $jpost_db_flickr_username;
			endif;
			$jpost_response = 'OK';
					$jpost_status = "Success";
					$jpost_http = 200;
					$jpost_status_text = 'OK';
					echo json_encode(array('status' => $jpost_status, 'removed'=>$jpost_ang_flickr_username, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered ),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
			
		break;
		case "update_selected_key":
			$jpost_filtered = array();
			$jpost_select = array();

			$jpost_data = json_decode(file_get_contents("php://input"));
			$jpost_ang_flickr_username['selected'] = array('key'=> $jpost_data->selected);

			$jpost_db_flickr_username = get_option( 'wpsimpleapi' . '_flickr-key' , false );
			if (is_array($jpost_db_flickr_username)) :
				$jpost_db_flickr_username['selected'] = $jpost_ang_flickr_username['selected'];
				update_option( 'wpsimpleapi' . '_flickr-key', $jpost_db_flickr_username );
			endif;
			if ($jpost_db_flickr_username != ''):
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				array_shift($jpost_db_flickr_username);
				foreach ($jpost_db_flickr_username as $user) {
					$jpost_filtered[] = array('key'=>$user['key']);
				}
			else:
				$jpost_filtered['selected'] = $jpost_db_flickr_username['selected'];
				$jpost_filtered[] = $jpost_db_flickr_username;
			endif;
			$jpost_response = 'OK';
			$jpost_status = "Success";
			$jpost_http = 200;
			$jpost_status_text = 'OK';
			echo json_encode(array('status' => $jpost_status,'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered ),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 200 OK");
		break;
		default:
		$jpost_response = "Not found";
		$jpost_status = "Error";
		$jpost_http = 500;
		$jpost_status_text = 'Not found';
		echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'recipes'=> 'none'),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header("HTTP/1.1 500 Internal server error");
		break;
		endswitch;
	endif;
}

function jpost_flickr_save() {
	// great response code utility library, not fully implemented @TODO
	require ('lib/http-response-codes.php');
	$actions = array("get_flickr_postmeta", "set_flickr_postmeta", "update_flickr_postmeta");
	if (isset($_GET["action"]) && in_array($_GET["action"], $actions)):
		switch($_GET["action"]):
		case 'set_flickr_postmeta':
			$jpost_filtered = array();
			$jpost_data = json_decode(file_get_contents("php://input"));
			$jpost_pid = $jpost_data->pid;
			foreach ($jpost_data->furls as $key=>$value) {
				$jpost_filtered[$key] = array('isprimary' => $value->isprimary, 'thumb' => $value->thumb, 'medium_640' => $value->medium_640, 'large_1600' => $value->large_1600, 'large_2048' => $value->large_2048);
			}
			// update_post_meta Wordpress utility function for updating postmeta table in wordpress database
			// See more: https://codex.wordpress.org/Function_Reference/update_post_meta
			$jpost_check = update_post_meta($jpost_pid, 'wpsimpleapi' . '_flickr-images', $jpost_filtered);
			if (isset($jpost_check) && ($jpost_check == 1 || $jpost_check > 0 )) {
				$jpost_response = 'OK';
				$jpost_status = "Success";
				$jpost_http = 200;
				$jpost_status_text = get_http_response($jpost_http, $textonly=1);
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'content'=> $jpost_filtered),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header(get_http_response($jpost_http));
			} else {
				$jpost_response = "Photo Set already added to Recipe";
				$jpost_status = "Conflict";
				$jpost_http = 409;
				$jpost_status_text = get_http_response($jpost_http, $textonly=1);
				echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'recipes'=> 'none'),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header(get_http_response($jpost_http));
			}
		break;
		default:
			$jpost_response = "Not found";
			$jpost_status = "Error";
			$jpost_http = 500;
			$jpost_status_text = get_http_response($jpost_http, $textonly=1);
			echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'recipes'=> 'none'),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header(get_http_response($jpost_http));
		break;
		endswitch;
	endif;
}
// function to retrive an array of posts from the wordpress database
function jpost_get_cpt() {
	global $post; 
	$jpost_title = array();
	$jpost_posts_args = array(
							'post_type' => 'recipe',
							'numberposts' => -1,
							'post_status' => 'publish',
							'fields' => 'ids'
	 					);
	// get_posts is a wordpress utility function for retriving post data from the wordpress posts table 
	// See more: https://codex.wordpress.org/Template_Tags/get_posts
	$jpost_posts = get_posts($jpost_posts_args);
	foreach ($jpost_posts as $key=>$value) {
		$jpost_title[$key] = array('value' => $value, 'label' => get_post_field('post_title', $value)); // get_post_field is a wordpress utility function for getting a small subset of data from the wordpress posts table
	}
	echo json_encode($jpost_title);
}
// function to authenticate user against Wordpress users table
function jpost_login() {
	global $post;
	require ('lib/http-response-codes.php');
	$jpost_data = json_decode(file_get_contents("php://input"));
	$jpost_username = $jpost_data->username;
	$jpost_password = $jpost_data->password;
	$jpost_csrf = $jpost_data->csrf_token;
	$today = date('D, d M Y H:i:s e');
	$now = time();
	$old_time = 1390837998;
	$hr_time_diff = get_time_elapsed($now-$old_time);

	// wp_verify_nonce is a security mechanism built into wordpress that generates unique tokens
	// used to authorize access to login function
	if( ! wp_verify_nonce( $jpost_csrf,'ang_token' ) ) {
				$jpost_response = "Invalid token";
				$jpost_status = "Error";
				$jpost_http = 500;
				$jpost_status_text = 'Internal server error';
	} else {

		if (isset($jpost_username) && isset($jpost_password) && isset($jpost_csrf)) {
			// wp_authenticate_username_password is a utility function for user authentication 
			// See more: http://wordpress.stackexchange.com/questions/61353/how-to-check-username-password-without-signing-in-the-user
			$wp_auth = wp_authenticate_username_password( NULL, $jpost_username, $jpost_password );
			if ( is_wp_error( $wp_auth ) ) {
				$jpost_response = "Login failed";
				$jpost_status = "Error";
				$jpost_http = 401;
				$jpost_status_text = get_http_response($jpost_http, $textonly=1);
			} else {
				$jpost_response = 'Login successful';
				$jpost_status = "Success";
				$jpost_http = 200;
				$jpost_status_text = get_http_response($jpost_http, $textonly=1);
			}
		} else {
			$jpost_response = "Invalid username or password";
			$jpost_status = "Error";
			$jpost_http = 401;
			$jpost_status_text = get_http_response($jpost_http, $textonly=1);
		}
	}
	echo json_encode(array('status' => $jpost_status, 'data'=> array('flash' => $jpost_response, 'recipes'=> 'none'),'headers'=>array('date' => $today, 'statusCode' => $jpost_http, 'statusText' => $jpost_status_text),'config'=>array('content-type'=>'application/json'))) . header(get_http_response($jpost_http));
}
// logout? @TODO
function jpost_logout() {

}